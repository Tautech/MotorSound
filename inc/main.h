

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_usart.h"


#include <stdio.h>

//typedef enum {DISABLE = 0, ENABLE = !FALSE} bool;
//typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;
//#define IS_FUNCTIONAL_STATE(STATE) (((STATE) == DISABLE) || ((STATE) == ENABLE))

#define FMCLK 25000000        // Master Clock On AD9833
#define AD9833PORT GPIOA      // PORT OF AD9833
#define AD9833DATA GPIO_PIN_6 // SPI DATA PIN
#define AD9833SCK GPIO_PIN_5  // SPI Clock PIN
#define AD9833SS GPIO_PIN_7   // SPI Chip Select
#define ASM_NOP() asm("NOP")  // Assembly NOPE (Little Delay)
//enum WaveType{SIN, SQR, TRI}; // Wave Selection Enum

#define	ADC1_DR_Address	((u32)0x4001244C)
#define	ADC2_DR_Address	((u32)0x4001284C)
#define	ADC3_DR_Address	((u32)0x40013C4C)
#define ADC_CDR_ADDRESS    ((uint32_t)0x40012308)

#define  ADC2_BUFFER_SIZE		9//1027	//128
#define	FFT_BUFFER_SIZE		1024


/*
#define QUEUE_BUFFER_LENGTH 1024

typedef struct 
{
    int head, tail, data;
    uint8_t Buffer[QUEUE_BUFFER_LENGTH];
}UARTQUEUE, *pUARTQUEUE;


extern UARTQUEUE WifiQueue;
*/

void main(void);
void GPIO_Configuration(void);
void Delay_ms(volatile uint32_t msec);
void Delay_us(volatile uint32_t usec);

void spi_con(void);
void writespi(uint16_t word);
void ad9833_setwave(uint16_t wave);
void ad9833_setwavedata(float frequency, float phase);

void  ADC_DMA_Config (void);
void ADC_INPUT(void);
void USART1_Config(void);
void USART6_Config(void);
void USART2_Config(void);


void init_I2C2(void);
void I2C_start(I2C_TypeDef* I2Cx, uint8_t address, uint8_t direction);
void I2C_write(I2C_TypeDef* I2Cx, uint8_t data);
uint8_t I2C_read_ack(I2C_TypeDef* I2Cx);
uint8_t I2C_read_nack(I2C_TypeDef* I2Cx);
void I2C_stop(I2C_TypeDef* I2Cx);

void ADC_Read(void);


/*
void USART1_IRQHandler(void);
void InitUartQueue(pUARTQUEUE pQueue);
void PutDataToUartQueue(pUARTQUEUE pQueue, uint8_t data);
uint8_t GetDataFromUartQueue(pUARTQUEUE pQueue);
*/
