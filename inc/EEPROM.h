#include "stm32f4xx.h"
#include <stdio.h>


void EEPROM_WRITE_Float(u16 nAddress, float Data);
float EEPROM_READ_Float(u16 nAddress);
void EEPROM_WRITE_Double(u16 nAddress, double Data);
double EEPROM_READ_Double(u16 nAddress);
void EEPROM_WRITE_DoubleWord(u16 nAddress, uint64_t Data);
uint64_t EEPROM_READ_DoubleWord(u16 nAddress);
void EEPROM_WRITE_Word(u16 nAddress, u32 Data);
u32 EEPROM_READ_Word(u16 nAddress);
void EEPROM_WRITE_HalfWord(u16 nAddress, u16 Data);
u16 EEPROM_READ_HalfWord(u16 nAddress);
void EEPROM_WRITE_Byte(u16 nAddress, u8 Data);
u8 EEPROM_READ_Byte(u16 nAddress);
u8 EEPROM_READY(void);
