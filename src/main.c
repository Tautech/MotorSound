#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include "stm32f4xx.h"
//#include "stm32f4xx_it.h"
#include "RA8875_Driver.h"
#include "GT911.h"
#include "main.h"
//#include "CT_I2C.h"
//#include "GPIO_config.h"


uint16_t FRQLW = 0;    // MSB of Frequency Tuning Word
uint16_t FRQHW = 0;    // LSB of Frequency Tuning Word
uint32_t  phaseVal=0;  // Phase Tuning Value
uint8_t WKNOWN=0;      // Flag Variable


uint8_t received_data[2];
#define 	ADC_ADDRESS 0x48 // the slave address (example)     1001 A2 A1 A0  => 1001001


//UARTQUEUE WifiQueue;


//__IO uint16_t	ADCConverteredValue[ADC2_BUFFER_SIZE];
__IO uint32_t	ADCConverteredValue[ADC2_BUFFER_SIZE];
					
/* Private typedef -----------------------------------------------------------*/

#ifdef __GNUC__
  	/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     	set to 'Yes') calls __io_putchar() */
  	#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  	#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */


/* Private define ------------------------------------------------------------*/
PUTCHAR_PROTOTYPE
{

#if 1//  USART6
  	/* Write a character to the USART */  
    	if( ch == '\n') {
        	USART_SendData(USART6, '\r');
        	while(USART_GetFlagStatus(USART6, USART_FLAG_TXE) == RESET );
        	USART_SendData(USART6, '\n');
    	}else {
        	USART_SendData(USART6, (uint8_t) ch);
    	}

    	/* Loop until the end of transmission */

    	while(USART_GetFlagStatus(USART6, USART_FLAG_TXE) == RESET );
		
#endif

   	return ch;
}

void main(void)
{
//	delay_init();
  	//GPIO_Configuration();
	//ADC_DMA_Config ();
	USART6_Config();

	printf("test1\n\r");


	LCD_Initializtion();
	LCD_Clear(White);

	delay_ms(300);
	GT911_TEST();


	while(1)
	{
		//ADC_INPUT();
		//GPIO_ResetBits(GPIOD,GPIO_Pin_13);
		//Delay_ms(1000);
		//GPIO_SetBits(GPIOD,GPIO_Pin_13);
		//Delay_ms(1000);

		//ad9833_setwavedata(2000000, 0);

	}
}




void USART6_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* USART1 Periph clock enable */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6, ENABLE);
	/* GPIOA Periph clock enable */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

	GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_USART6);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_USART6);

	/* Enable the USART1 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART6_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 5;

	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);		
	//NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x10000);
	
	/* Configure the USART1_Tx */
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	  
	/* Configure the USART1_Rx as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;   
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	USART_InitStructure.USART_BaudRate = 115200;	//921600; //460800; // 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART6, &USART_InitStructure);
	USART_ITConfig(USART6, USART_IT_RXNE, ENABLE);
	/* Enable the USART1 */
	USART_Cmd(USART6, ENABLE);
}









#ifdef USE_FULL_ASSERT
/**
  * @brief  assert_failed
  *         Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  File: pointer to the source file name
  * @param  Line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
  ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {}
}
#endif
