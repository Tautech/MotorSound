#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include<math.h>
#include "stm32f4xx.h"
#include "main.h"
#include "EEPROM.h"




void EEPROM_WRITE_Float(u16 nAddress, float Data)
{
	u32		DumpData;

	memcpy(&DumpData, &Data, 4);
	EEPROM_WRITE_Word( nAddress, DumpData );


}

float EEPROM_READ_Float(u16 nAddress)
{
	float temp = 0;
	u32	DumpData;

	DumpData = EEPROM_READ_Word( nAddress );
	memcpy( &temp,(void*)&DumpData , 4 );
//	printf("temp = %f\r\n",temp);

	return temp;
}


void EEPROM_WRITE_Double(u16 nAddress, double Data)
{
	uint64_t		DumpData;
	memcpy(&DumpData, &Data, 8);

	EEPROM_WRITE_DoubleWord(nAddress, DumpData);
}


double EEPROM_READ_Double(u16 nAddress)
{
	double temp=0;
	uint64_t	DumpData;
	
	DumpData = EEPROM_READ_DoubleWord( nAddress );
	memcpy( &temp,(void*)&DumpData , 8 );
	return temp; // return received data from SPI data register

}


void EEPROM_WRITE_DoubleWord(u16 nAddress, uint64_t Data)
{
	while(EEPROM_READY());

	AT25LC320_Enable;
	Delay_us(10);
	SPI1_send(WRITE_ENABLE);
	Delay_us(10);
	AT25LC320_Disable;
	Delay_us(10);
	AT25LC320_Enable;
	Delay_us(10);

	SPI1_send(DATA_WRITE);
	SPI1_send((u8)(nAddress>>8));
	SPI1_send((u8)(nAddress));
	SPI1_send((u8)(Data));
	SPI1_send((u8)(Data>>8));
	SPI1_send((u8)(Data>>16));
	SPI1_send((u8)(Data>>24));
	SPI1_send((u8)(Data>>32));
	SPI1_send((u8)(Data>>40));
	SPI1_send((u8)(Data>>48));
	SPI1_send((u8)(Data>>56));

	Delay_us(10);
	AT25LC320_Disable;
	Delay_us(10);
	AT25LC320_Enable;
	Delay_us(10);
	SPI1->DR = WRITE_DISABLE;
	while( !(SPI1->SR & SPI_I2S_FLAG_TXE) ); // wait until transmit complete
	Delay_us(10);
	AT25LC320_Disable;
}


uint64_t EEPROM_READ_DoubleWord(u16 nAddress)
{
	uint64_t	temp=0;
	
	while(EEPROM_READY());

	AT25LC320_Enable;
	Delay_us(10);
	SPI1_send(DATA_READ);

	SPI1_send((u8)(nAddress>>8));
	SPI1_send((u8)(nAddress));

	temp |= SPI1_send(0x00);
	temp |= (uint64_t)SPI1_send(0x00) << 8;
	temp |= (uint64_t)SPI1_send(0x00) << 16;
	temp |= (uint64_t)SPI1_send(0x00) << 24;
	temp |= (uint64_t)SPI1_send(0x00) << 32;
	temp |= (uint64_t)SPI1_send(0x00) << 40;
	temp |= (uint64_t)SPI1_send(0x00) << 48;
	temp |= (uint64_t)SPI1_send(0x00) << 56;

	Delay_us(10);
	AT25LC320_Disable;
	return temp; // return received data from SPI data register

}


void EEPROM_WRITE_Word(u16 nAddress, u32 Data)
{
	while(EEPROM_READY());

	AT25LC320_Enable;
	Delay_us(10);
	SPI1_send(WRITE_ENABLE);
	Delay_us(10);
	AT25LC320_Disable;
	Delay_us(10);
	AT25LC320_Enable;
	Delay_us(10);

	SPI1_send(DATA_WRITE);
	SPI1_send((u8)(nAddress>>8));
	SPI1_send((u8)(nAddress));
	SPI1_send((u8)(Data));
	SPI1_send((u8)(Data>>8));
	SPI1_send((u8)(Data>>16));
	SPI1_send((u8)(Data>>24));

	Delay_us(10);
	AT25LC320_Disable;
	Delay_us(10);
	AT25LC320_Enable;
	Delay_us(10);
	SPI1->DR = WRITE_DISABLE;
	while( !(SPI1->SR & SPI_I2S_FLAG_TXE) ); // wait until transmit complete
	Delay_us(10);
	AT25LC320_Disable;
}


u32 EEPROM_READ_Word(u16 nAddress)
{
	u32 temp=0;
	while(EEPROM_READY());

	AT25LC320_Enable;
	Delay_us(10);
	SPI1_send(DATA_READ);

	SPI1_send((u8)(nAddress>>8));
	SPI1_send((u8)(nAddress));

	temp = SPI1_send(0x00);
	temp |= (u32)( SPI1_send(0x00) << 8);
	temp |= (u32)( SPI1_send(0x00) << 16);
	temp |= (u32)( SPI1_send(0x00) << 24);

	Delay_us(10);
	AT25LC320_Disable;
	return temp; // return received data from SPI data register

}


void EEPROM_WRITE_HalfWord(u16 nAddress, u16 Data)
{
	while(EEPROM_READY());

	AT25LC320_Enable;
	Delay_us(10);
	SPI1_send(WRITE_ENABLE);
	Delay_us(10);
	AT25LC320_Disable;
	Delay_us(10);
	AT25LC320_Enable;
	Delay_us(10);

	SPI1_send(DATA_WRITE);
	SPI1_send((u8)(nAddress>>8));
	SPI1_send((u8)(nAddress));
	SPI1_send((u8)(Data));
	SPI1_send((u8)(Data>>8));

	Delay_us(10);
	AT25LC320_Disable;
	Delay_us(10);
	AT25LC320_Enable;
	Delay_us(10);
	SPI1->DR = WRITE_DISABLE;
	while( !(SPI1->SR & SPI_I2S_FLAG_TXE) ); // wait until transmit complete
	Delay_us(10);
	AT25LC320_Disable;
}


u16 EEPROM_READ_HalfWord(u16 nAddress)
{
	u16 temp=0;
	while(EEPROM_READY());

	AT25LC320_Enable;
	Delay_us(10);
	SPI1_send(DATA_READ);

	SPI1_send((u8)(nAddress>>8));
	SPI1_send((u8)(nAddress));

	temp = SPI1_send(0x00);
	temp |= (u16)( SPI1_send(0x00) << 8);

	Delay_us(10);
	AT25LC320_Disable;
	return temp; // return received data from SPI data register

}


void EEPROM_WRITE_Byte(u16 nAddress, u8 Data)
{
	while(EEPROM_READY());

	AT25LC320_Enable;
	Delay_us(10);
	SPI1_send(WRITE_ENABLE);
	Delay_us(10);
	AT25LC320_Disable;
	Delay_us(10);
	AT25LC320_Enable;
	Delay_us(10);

	SPI1_send(DATA_WRITE);
	SPI1_send((u8)(nAddress>>8));
	SPI1_send((u8)(nAddress));
	SPI1_send(Data);
	
	Delay_us(10);
	AT25LC320_Disable;
	Delay_us(10);
	AT25LC320_Enable;
	Delay_us(10);

	SPI1_send(WRITE_DISABLE);

	Delay_us(10);
	AT25LC320_Disable;
}


u8 EEPROM_READ_Byte(u16 nAddress)
{
	u8 temp=0;
	while(EEPROM_READY());

	AT25LC320_Enable;
	Delay_us(10);
	SPI1_send(DATA_READ);
	SPI1_send((u8)(nAddress>>8));
	SPI1_send((u8)(nAddress));

	temp = SPI1_send(0x00);
	Delay_us(10);
	AT25LC320_Disable;
	return temp; // return received data from SPI data register

}

u8 EEPROM_READY(void)
{
	u8 temp=0;
	
	AT25LC320_Enable;
	//Delay_us(10);
	SPI1_send(READ_STATUS);
	//Delay_us(10);
	temp = SPI1_send(0x00);
	//Delay_us(10);
	AT25LC320_Disable;
	return temp;
	
}

